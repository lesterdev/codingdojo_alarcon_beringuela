import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';


describe('AppComponent', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [RouterTestingModule],
    declarations: [AppComponent]
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'tic-tac-toe'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('tic-tac-toe');
  });

  it(`one two three player One wins'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    app.ticTacToeList[0].isSelected = true;
    app.ticTacToeList[1].isSelected = true;
    app.ticTacToeList[2].isSelected = true;

    app.ticTacToeList[0].playerId = 1;
    app.ticTacToeList[1].playerId = 1;
    app.ticTacToeList[2].playerId = 1;

    expect(app.ticTacToeList[0].isSelected).toBe(true);
    expect(app.ticTacToeList[1].isSelected).toBe(true);
    expect(app.ticTacToeList[2].isSelected).toBe(true);
  });

  it(`one two three player Two wins'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    app.ticTacToeList[0].isSelected = true;
    app.ticTacToeList[1].isSelected = true;
    app.ticTacToeList[2].isSelected = true;

    app.ticTacToeList[0].playerId = 2;
    app.ticTacToeList[1].playerId = 2;
    app.ticTacToeList[2].playerId = 2;

    expect(app.ticTacToeList[0].isSelected).toBe(true);
    expect(app.ticTacToeList[1].isSelected).toBe(true);
    expect(app.ticTacToeList[2].isSelected).toBe(true);
  });

  it(`one five nine player One wins'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    app.ticTacToeList[0].isSelected = true;
    app.ticTacToeList[4].isSelected = true;
    app.ticTacToeList[8].isSelected = true;

    app.ticTacToeList[0].playerId = 1;
    app.ticTacToeList[4].playerId = 1;
    app.ticTacToeList[8].playerId = 1;

    expect(app.ticTacToeList[0].isSelected).toBe(true);
    expect(app.ticTacToeList[4].isSelected).toBe(true);
    expect(app.ticTacToeList[8].isSelected).toBe(true);
  });

  it(`one five nine player Two wins'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    app.ticTacToeList[0].isSelected = true;
    app.ticTacToeList[4].isSelected = true;
    app.ticTacToeList[8].isSelected = true;

    app.ticTacToeList[0].playerId = 2;
    app.ticTacToeList[4].playerId = 2;
    app.ticTacToeList[8].playerId = 2;

    expect(app.ticTacToeList[0].isSelected).toBe(true);
    expect(app.ticTacToeList[4].isSelected).toBe(true);
    expect(app.ticTacToeList[8].isSelected).toBe(true);
  });
});
