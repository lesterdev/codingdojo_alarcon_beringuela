import { Component } from '@angular/core';
import { ITicTacToe } from './TicTacToeInterface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  public outPut: string = '';
  title = 'tic-tac-toe';
  public isPlayerOne: boolean = true;
  public ticTacToeList: ITicTacToe[] = [
    { value: 1, isSelected: false, playerId: 0 },
    { value: 2, isSelected: false, playerId: 0 },
    { value: 3, isSelected: false, playerId: 0 },
    { value: 4, isSelected: false, playerId: 0 },
    { value: 5, isSelected: false, playerId: 0 },
    { value: 6, isSelected: false, playerId: 0 },
    { value: 7, isSelected: false, playerId: 0 },
    { value: 8, isSelected: false, playerId: 0 },
    { value: 9, isSelected: false, playerId: 0 },
  ];

  public toggleBtn(element: any, selectedList: ITicTacToe) {
    const buttonValue = this.isPlayerOne ? 'X' : 'O';
    element.textContent = buttonValue;
    selectedList.playerId = this.isPlayerOne ? 1 : 2;
    this.isPlayerOne = !this.isPlayerOne;
    element.disabled = true;
    selectedList.isSelected = true;
    this.ticTacToeOperation();
  }
  public ticTacToeOperation() {
    if (
      this.ticTacToeList[0].isSelected &&
      this.ticTacToeList[1].isSelected &&
      this.ticTacToeList[2].isSelected
    ) {
      if (
        this.ticTacToeList[0].playerId === 1 &&
        this.ticTacToeList[1].playerId === 1 &&
        this.ticTacToeList[2].playerId === 1
      ) {
        this.outPut = 'PLAYER ONE WINS';
      } else if (
        this.ticTacToeList[0].playerId === 2 &&
        this.ticTacToeList[1].playerId === 2 &&
        this.ticTacToeList[2].playerId === 2
      ) {
        this.outPut = 'PLAYER TWO WINS';
      }
    }
    if (
      this.ticTacToeList[0].isSelected &&
      this.ticTacToeList[3].isSelected &&
      this.ticTacToeList[6].isSelected
    ) {
      if (
        this.ticTacToeList[0].playerId === 1 &&
        this.ticTacToeList[3].playerId === 1 &&
        this.ticTacToeList[6].playerId === 1
      ) {
        this.outPut = 'PLAYER ONE WINS';
      } else if (
        this.ticTacToeList[0].playerId === 2 &&
        this.ticTacToeList[3].playerId === 2 &&
        this.ticTacToeList[6].playerId === 2
      ) {
        this.outPut = 'PLAYER TWO WINS';
      }
    }
    if (
      this.ticTacToeList[0].isSelected &&
      this.ticTacToeList[4].isSelected &&
      this.ticTacToeList[8].isSelected
    ) {
      console.log(1, 5, 9);
      if (
        this.ticTacToeList[0].playerId === 1 &&
        this.ticTacToeList[4].playerId === 1 &&
        this.ticTacToeList[8].playerId === 1
      ) {
        this.outPut = 'PLAYER ONE WINS';
      } else if (
        this.ticTacToeList[0].playerId === 2 &&
        this.ticTacToeList[4].playerId === 2 &&
        this.ticTacToeList[8].playerId === 2
      ) {
        this.outPut = 'PLAYER TWO WINS';
      }
    }
    if (
      this.ticTacToeList[1].isSelected &&
      this.ticTacToeList[4].isSelected &&
      this.ticTacToeList[7].isSelected
    ) {
      if (
        this.ticTacToeList[1].playerId === 1 &&
        this.ticTacToeList[4].playerId === 1 &&
        this.ticTacToeList[7].playerId === 1
      ) {
        this.outPut = 'PLAYER ONE WINS';
      } else if (
        this.ticTacToeList[1].playerId === 2 &&
        this.ticTacToeList[4].playerId === 2 &&
        this.ticTacToeList[7].playerId === 2
      ) {
        this.outPut = 'PLAYER TWO WINS';
      }
    }
    if (
      this.ticTacToeList[2].isSelected &&
      this.ticTacToeList[4].isSelected &&
      this.ticTacToeList[6].isSelected
    ) {
      if (
        this.ticTacToeList[2].playerId === 1 &&
        this.ticTacToeList[4].playerId === 1 &&
        this.ticTacToeList[6].playerId === 1
      ) {
        this.outPut = 'PLAYER ONE WINS';
      } else if (
        this.ticTacToeList[2].playerId === 2 &&
        this.ticTacToeList[4].playerId === 2 &&
        this.ticTacToeList[6].playerId === 2
      ) {
        this.outPut = 'PLAYER TWO WINS';
      }
    }
    if (
      this.ticTacToeList[2].isSelected &&
      this.ticTacToeList[5].isSelected &&
      this.ticTacToeList[8].isSelected
    ) {
      if (
        this.ticTacToeList[2].playerId === 1 &&
        this.ticTacToeList[5].playerId === 1 &&
        this.ticTacToeList[8].playerId === 1
      ) {
        this.outPut = 'PLAYER ONE WINS';
      } else if (
        this.ticTacToeList[2].playerId === 2 &&
        this.ticTacToeList[5].playerId === 2 &&
        this.ticTacToeList[8].playerId === 2
      ) {
        this.outPut = 'PLAYER TWO WINS';
      }
    }
    if (
      this.ticTacToeList[3].isSelected &&
      this.ticTacToeList[5].isSelected &&
      this.ticTacToeList[4].isSelected
    ) {
      if (
        this.ticTacToeList[3].playerId === 1 &&
        this.ticTacToeList[5].playerId === 1 &&
        this.ticTacToeList[4].playerId === 1
      ) {
        this.outPut = 'PLAYER ONE WINS';
      } else if (
        this.ticTacToeList[3].playerId === 2 &&
        this.ticTacToeList[5].playerId === 2 &&
        this.ticTacToeList[4].playerId === 2
      ) {
        this.outPut = 'PLAYER TWO WINS';
      }
    }
    if (
      this.ticTacToeList[6].isSelected &&
      this.ticTacToeList[7].isSelected &&
      this.ticTacToeList[8].isSelected
    ) {
      if (
        this.ticTacToeList[6].playerId === 1 &&
        this.ticTacToeList[7].playerId === 1 &&
        this.ticTacToeList[8].playerId === 1
      ) {
        this.outPut = 'PLAYER ONE WINS';
      } else if (
        this.ticTacToeList[6].playerId === 2 &&
        this.ticTacToeList[7].playerId === 2 &&
        this.ticTacToeList[8].playerId === 2
      ) {
        this.outPut = 'PLAYER TWO WINS';
      }
    }
    const isAllFieldsTaken = this.ticTacToeList.filter(t => t.isSelected).length >= 9;

    if (isAllFieldsTaken && this.outPut === '')
    {
      this.outPut = "Game Over";
    }
    console.log(this.ticTacToeList);
  }
}
